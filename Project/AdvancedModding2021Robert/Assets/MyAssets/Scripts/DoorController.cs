using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    private Animator _animator;
    [SerializeField] AudioSource doorAudio;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            _animator.SetBool("Open", true);
            doorAudio.Play();
            print ("sound");
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    void PlaySound(AudioClip soundClip)
    {
        doorAudio.PlayOneShot(soundClip);
        print("updatesound");
    }
}
