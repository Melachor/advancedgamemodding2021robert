using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.AI;



public class Basic_AI : MonoBehaviour
{

    public enum Behaviors { Wandering, Fight }
    //public enum Hive { Wasps, Bees, Hornets, Fly }


    [Header("Agent Setup")]
    //[SerializeField] Hive hiveLoyalty;
    Transform currentEnemy;

    [SerializeField] Behaviors currentBehavior;
    [SerializeField] NavMeshAgent aiAgent;
    //[SerializeField] Transform foodTarget;

    /*[Header("Foodies")]
    [HideInInspector] public Transform home;
    [SerializeField] bool hasFood = false;
    [SerializeField] float hunger = 100f;
    [SerializeField] float hungerRate = 1f;
    [SerializeField] float hungerTreshold = 70;
*/


    [Header("Health")]
    //[SerializeField] float hp = 100f;
    //[SerializeField] float def = 20;
    [SerializeField] int dam = 10;
    //[SerializeField] float damRate = 0.5f;
    private float timer = 1f;
    //private bool canAttack;


    [Header("Waypoints")]
    public WayPointManager wm;
    [SerializeField] float desTreshold = 1f;

    List<Transform> wayPoints;
    int currentWayPoint = 0;

    // Start is called before the first frame update


    void Start()
    {

        if (aiAgent == null)
        {
            GetComponent<NavMeshAgent>();
        }

        wayPoints = wm.wayPoints;

    }

    // Update is called once per frame
    void Update()
    {

        /*if (Timer(3f))
        {
            canAttack = true;
        }
        else
        {
            canAttack = false;
        }*/

        switch (currentBehavior)
        {

            case Behaviors.Wandering:
                Wandering();
                break;

            case Behaviors.Fight:
                Fighting();
                break;

        }

    }
    void Wandering()
    {



        if (wayPoints == null)
        {
            return;
        }
        if (aiAgent.remainingDistance <= desTreshold)
        {
            currentWayPoint = (currentWayPoint + 1) % wayPoints.Count;

            aiAgent.SetDestination(wayPoints[currentWayPoint].position);
        }
        Debug.DrawLine(transform.position, wayPoints[currentWayPoint].position, Color.green);
    }


    void Fighting()
    {
        if (currentEnemy == null)
        {
            currentBehavior = Behaviors.Wandering;

        }
        aiAgent.SetDestination(currentEnemy.position);
        if (aiAgent.remainingDistance <= desTreshold && Timer(3f))
        {
            print("totally hit him");
            currentEnemy.GetComponent<PlayerCondition>().healthPoints -= dam;


        }

    }
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")

        {
            currentBehavior = Behaviors.Fight;
            currentEnemy = col.transform;
        }


    }
    bool Timer(float sec)
    {
        timer = timer +1 * Time.deltaTime;

        if (timer >= sec)
        {

            timer = 0f;

            return true;

        }
        else
        {

            return false;

        }
    }
}

