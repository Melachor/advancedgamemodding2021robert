using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SetMenu : MonoBehaviour
{

    public GameObject canvasMainMenu;
    public GameObject canvasOptionsMenu;
    public GameObject canvasPauseMenu;
    

    public void PauseToOptions()
    {
        canvasPauseMenu.SetActive(false);
        canvasOptionsMenu.SetActive(true);
    }

    public void OptionsToPause()
    {
        canvasPauseMenu.SetActive(true);
        canvasOptionsMenu.SetActive(false);
    }

    public void OptionsMenu()
    {
        canvasMainMenu.SetActive(false);
        canvasOptionsMenu.SetActive(true);
    }
    public void OptionsToMenu()
    {
        canvasOptionsMenu.SetActive(false);
        canvasMainMenu.SetActive(true);
    }

    public void Quit()
    {
        Application.Quit();

    }
    public void LoadThisScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
