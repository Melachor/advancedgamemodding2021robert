using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour
{
    public GameObject canvasPauseMenu;
    public AudioSource soundUI;
    public bool gamePaused;


    void Start()
    {
        gamePaused = false;
    }


    void Update()
    {
        PauseGame();
        if (gamePaused)
        {
            Time.timeScale = 0;
        }
        if (!gamePaused)
        {
            Time.timeScale = 1;
        }
    }
    private void PauseGame()
    {
        if (Input.GetButtonDown("Pause"))
        {
            gamePaused = true;
            canvasPauseMenu.SetActive(true);
             Cursor.lockState = CursorLockMode.None;
            
        }
       
    }
    public void ResumeGame()
    {
        soundUI.Play();
        gamePaused = false;
        canvasPauseMenu.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;



    }
}
